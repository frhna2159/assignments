function validateForm() {
  		let q1 = document.forms["quiz"]["question1"].value;
  		let q2 = document.forms["quiz"]["question2"].value;
  		let q3 = document.forms["quiz"]["question3"].value;
  		let q4 = document.forms["quiz"]["question4"].value;
  		let q5 = document.forms["quiz"]["question5"].value;
		let q6 = document.forms["quiz"]["question6"].value;
		let q7 = document.forms["quiz"]["question7"].value;
		let q8 = document.forms["quiz"]["question8"].value;
		let q9 = document.forms["quiz"]["question9"].value;
		let q10 = document.forms["quiz"]["question10"].value;
  		
  		if (q1 == "") {
    		alert("Question 1 must be filled out");
    		return false;
  		}

  		if (q2 == "") {
    		alert("Question 2 must be filled out");
    		return false;
  		}

  		if (q3 == "") {
    		alert("Question 3 must be filled out");
    		return false;
  		}

  		if (q4 == "") {
    		alert("Question 4 must be filled out");
    		return false;
  		}

  		if (q5 == "") {
    		alert("Question 5 must be filled out");
    		return false;
  		}
		
		if (q6 == "") {
    		alert("Question 6 must be filled out");
    		return false;
  		}

		if (q7 == "") {
    		alert("Question 7 must be filled out");
    		return false;
  		}

		if (q8 == "") {
    		alert("Question 8 must be filled out");
    		return false;
  		}

		if (q9 == "") {
    		alert("Question 9 must be filled out");
    		return false;
  		}

		if (q10 == "") {
    		alert("Question 10 must be filled out");
    		return false;
  		}
	}

	function check(){
	var score=0;
	var q1=document.quiz.question1.value;
	var q2=document.quiz.question2.value;
	var q3=document.quiz.question3.value;
	var q4=document.quiz.question4.value;
	var q5=document.quiz.question5.value;
	var q6=document.quiz.question6.value;
	var q7=document.quiz.question7.value;
	var q8=document.quiz.question8.value;
	var q9=document.quiz.question9.value;
	var q10=document.quiz.question10.value;
	var name = document.getElementById("yname").value;

	if (q1=="cascading") { score++;}
	if (q2=="style") { score++;}
	if (q3=="background-color") { score++;}
	if (q4=="text-color") { score++;}
	if (q5=="text-size") { score++;}
	if (q6=="weight") { score++;}
	if (q7=="coma") { score++;}
	if (q8=="static") { score++;}
	if (q9=="#demo") { score++;}
	if (q10=="no") { score++;}

	if (score < 5) {
		alert("Keep trying, " + name + "! You answeres " + score + " out of 10 correctly")
	}

	else if (score < 10) {
		alert("Way to go, " + name + "! You got " + score + " out of 10 correct")
	}

	else{
		alert("Congratulations " + name + "! You got " + score + " out of 10")
	}

	quiz.style.display="none";


}
